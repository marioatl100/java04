package deloitte.academy.lesson01.entity;

/**
 * Clase abstracta Hotel int habitacionesDisponibles int costo String nombre
 * 
 * @author mvillegas
 *
 */
public abstract class Hotel {
	private int habitacionesDisponibles;
	private int costo;
	private String nombre;
	private Huesped huesped;

	public Hotel(int habitacionesDisponibles, int costo, String nombre, Huesped huesped) {
		super();
		this.habitacionesDisponibles = habitacionesDisponibles;
		this.costo = costo;
		this.nombre = nombre;
		this.huesped = huesped;
	}

	public int getHabitacionesDisponibles() {
		return habitacionesDisponibles;
	}

	public void setHabitacionesDisponibles(int habitacionesDisponibles) {
		this.habitacionesDisponibles = habitacionesDisponibles;
	}

	public int getCosto() {
		return costo;
	}

	public void setCosto(int costo) {
		this.costo = costo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Hotel() {
		// TODO Auto-generated constructor stub
	}

	public Hotel(int habitacionesDisponibles) {
		super();
		this.habitacionesDisponibles = habitacionesDisponibles;
	}

	public Huesped getHuesped() {
		return huesped;
	}

	public void setHuesped(Huesped huesped) {
		this.huesped = huesped;
	}

	/**
	 * funcion cobrar regresa un double que es el valor cobrado
	 * 
	 * @return
	 */
	public abstract double cobrar();

}
