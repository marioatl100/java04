package deloitte.academy.lesson01.entity;

/**
 * Clase cliente hereda atributos y metodos de la clase hotel y contiene la
 * funcion costo
 * 
 * @author mvillegas
 *
 */
public class Cliente extends Hotel {

	@Override
	public double cobrar() {
		// TODO Auto-generated method stub
		return this.getCosto();

	}

	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cliente(int habitacionesDisponibles, int costo, String nombre, Huesped huesped) {
		super(habitacionesDisponibles, costo, nombre, huesped);
		// TODO Auto-generated constructor stub
	}

}
