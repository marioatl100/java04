package deloitte.academy.lesson01.entity;

/**
 * Clase clienteFrecuente hereda de la clase hotel Contiene la funcion cobrar
 * 
 * @author mvillegas
 *
 */
public class ClienteFrecuente extends Hotel {

	@Override
	public double cobrar() {
		// TODO Auto-generated method stub
		return this.getCosto() * .85;
	}

	public ClienteFrecuente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClienteFrecuente(int habitacionesDisponibles, int costo, String nombre, Huesped huesped) {
		super(habitacionesDisponibles, costo, nombre, huesped);
		// TODO Auto-generated constructor stub
	}

}
