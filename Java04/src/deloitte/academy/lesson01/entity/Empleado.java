package deloitte.academy.lesson01.entity;
/**
 * La clase Empleado hereda atributosy metodos
 *  de clase hote
 *  
 *  Tiene funcion cobrar
 * @author mvillegas
 *
 */
public class Empleado extends Hotel {

	@Override
	public double cobrar() {
		// TODO Auto-generated method stub
		return this.getCosto() * .70;
	}

	public Empleado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Empleado(int numHab, int costo, String nombre, Huesped huesped) {
		super(numHab, costo, nombre, huesped);
		// TODO Auto-generated constructor stub
	}

	public String tipo() {
		return Huesped.Empleado.toString();
	}

}
