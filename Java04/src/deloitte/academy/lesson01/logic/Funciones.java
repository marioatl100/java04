package deloitte.academy.lesson01.logic;

import deloitte.academy.lesson01.entity.Habitaciones;
import deloitte.academy.lesson01.entity.Hotel;
import deloitte.academy.lesson01.run.Main;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.*;

/**
 * Clase que contiene las funciones de checkIn y checkOut
 * 
 * @author mvillegas
 *
 */
public class Funciones {
	private static final Logger LOGGER = Logger.getLogger(Funciones.class.getName());;

	/**
	 * Funcion checkIn registra un huesped dentro del hotel
	 * 
	 * @param hotel
	 */
	public static void checkIn(Hotel hotel) {

		if (Main.habitaciones[hotel.getHabitacionesDisponibles()] == 1) {
			LOGGER.info("Cliente registrado satisfactoriamente");
			System.out.println("Precio original: " + hotel.getCosto() + "  Precio con descuento de "
					+ hotel.getHuesped().toString() + ": " + hotel.cobrar());
			Main.listaOficial.add(hotel);
			Main.habitaciones[hotel.getHabitacionesDisponibles() - 1] = 0;
		} else {
			LOGGER.log(Level.SEVERE, "Habitacion ocupada");

		}
	}

	/**
	 * Funcion CheckOut da de baja un cliente del hotel recibiendo al cliente
	 * 
	 * @param hotel
	 */
	public static void checkOut(Hotel hotel) {
		if (Main.habitaciones[hotel.getHabitacionesDisponibles()] == 0) {
			LOGGER.info("Cliente dado de baja satisfactoriamente");

			Main.listaOficial.remove(hotel);
			Main.habitaciones[hotel.getHabitacionesDisponibles()] = 1;
		} else {
			LOGGER.log(Level.SEVERE, "Este huesped no esta hospedad");

		}
	}
}
