package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;

import deloitte.academy.lesson01.entity.Cliente;
import deloitte.academy.lesson01.entity.ClienteFrecuente;
import deloitte.academy.lesson01.entity.Empleado;
import deloitte.academy.lesson01.entity.Habitaciones;
import deloitte.academy.lesson01.entity.Hotel;
import deloitte.academy.lesson01.entity.Huesped;
import deloitte.academy.lesson01.logic.Funciones;

/**
 * Clase main con la funcionalidad y variables globales de proyecto
 * 
 * @author mvillegas
 *
 */
public class Main {
	public static List<Hotel> listaOficial = new ArrayList<Hotel>();
	//public static ArrayList<Boolean> disponibles = new ArrayList<Boolean>();
	public static int[] habitaciones = new int[20];

	public static void main(String[] args) {
		Cliente a2 = new Cliente(3, 5000, "Mario", Huesped.Cliente);
		Empleado e1 = new Empleado(5, 5000, "Hugo", Huesped.Empleado);
		ClienteFrecuente cf1 = new ClienteFrecuente(7, 5000, "Ruben", Huesped.Cliente_Frecuente);

		// Funciones.checkIn(a2);
		// disponibles.get(3).valueOf(false);
		// habitaciones[a2.getHabitacionesDisponibles()]=0;
		int contador = 1;
		Boolean a = false;
		Habitaciones aux = new Habitaciones(0);
		for (int i = 0; i < 20; i++) {
			aux.setHabitacionesDisponibles(i);
			listaOficial.add(aux);
			habitaciones[i] = 1;
		}

		for (int aux2 : habitaciones) {

			if (aux2 == 1) {
				System.out.print("1  ");
			} else {
				System.out.print("0  ");
			}
			if (contador % 10 == 0) {
				System.out.println("");
				System.out.println("----------------------------");
			}
			contador++;

		}
		// habitaciones[a2.getHabitacionesDisponibles()]=0;
		Funciones.checkIn(a2);
		Funciones.checkIn(e1);
		Funciones.checkIn(cf1);
		for (int aux2 : habitaciones) {

			if (aux2 == 1) {
				System.out.print("1  ");
			} else {
				System.out.print("0  ");
			}
			if (contador % 10 == 0) {
				System.out.println("");
				System.out.println("----------------------------");
			}
			contador++;

		}
		Funciones.checkIn(a2);
		Funciones.checkOut(a2);
		for (int aux2 : habitaciones) {

			if (aux2 == 1) {
				System.out.print("1  ");
			} else {
				System.out.print("0  ");
			}
			if (contador % 10 == 0) {
				System.out.println("");
				System.out.println("----------------------------");
			}
			contador++;

		}

		// TODO Auto-generated method stub

	}

}
